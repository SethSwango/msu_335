/*
 Name: Seth Swango
 Date: 01/31/2015
 Assignment: Dice Race
 Platform/IDE: OSX/Xcode
 Description: This program is a command line game called dice race. It has two players, jack and jill which are represented by class bojects. When you run the program, both 'players' are completely automated and will automatically compete until one wins. Players compete by taking turns by rolling a 6-sided die and move forward the number of squares shown. If a player lands on the same square as his or her opponent, the opponent tumbles back a random number of 1 – 3 squares. The first player to reach the "top" of the hill (square 20) wins. You, the user, can enter in a custom number of games to be played. When all the games have been played the player that won the most games will be announced!
 */

#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::ostream;
using std::string;

int roll() {
    int roll = rand() % 6 + 1;
    return roll;
}

class Player {
public:
    // constructors
    // no default constructor necessary because
    // all player objects are hard coded in main()
    Player(string name, int position, int lastRoll, int gamesWon);
    
    // setters
    void move();
    void tumble();
    void win();
    void resetPosition();
    
    // getters
    int getPosition();
    string getName();
    int getLastRoll();
    int getGamesWon();
    
    // == overload
    bool operator== (const Player& rhs) const;
    
private:
    int position;
    string name;
    int lastRoll;
    int gamesWon;
    
};

//Constructor -- no defualt necessary for this application
Player::Player(string aName, int aPosition, int aLastRoll, int aGamesWon) {
    name = aName;
    position = aPosition;
    lastRoll = aLastRoll;
    gamesWon = aGamesWon;
}

//SETTERS
//called for each player turn
void Player::move() {
    lastRoll = roll();
    position+=lastRoll;
}

//called for each player tumble
void Player::tumble() {
    position-=3;
}

//called when a player wins
void Player::win() {
    gamesWon+=1;
}

//called at the end of game
void Player::resetPosition(){
    position=0;
}

//GETTERS
//used to get player position
int Player::getPosition() {
    return position;
}

//used to get player name
string Player::getName() {
    return name;
}

//used to get last roll
int Player::getLastRoll() {
    return lastRoll;
}

//used to get number of games won
int Player::getGamesWon() {
    return gamesWon;
}

// overload for "==" operator
bool Player::operator== (const Player& rhs) const {
    if (position == rhs.position)
        return true;
    else
        return false;
}

//print the game play
void printBoard(){
    
    srand(int(time(NULL))); //seed the randomizer
    
    Player jack = Player("Jack",0,0,0);
    Player jill = Player("Jill",0,0,0);
    
    int gameCount = 0;
    int gamesToBePlayed; //allow the user to set the number of games to be played
    cout << "Please enter an odd number of games to be played!\n";
    cin >> gamesToBePlayed;
    
    while (gameCount < gamesToBePlayed) {
        cout << "Roll    Jack    Jill\n";
        cout << "-------------------------------\n";
        
        do {
            
            jack.move(); //move jack across the board
            
            //print for when tumble is necessary
            if (jack == jill) {
                jill.tumble();
                cout << jack.getLastRoll() << "\t\t" << jack.getPosition();
                cout << "\t\t" << jill.getPosition() << "\t\t" << "Tumble Jill!" << "\n";
            }
            
            //print for when tumble isn't necessary
            else {
                cout << jack.getLastRoll() << "\t\t" << jack.getPosition() << "\n";
            }
            
            jill.move();
            
            //print for when tumble is necessary
            if (jack == jill) {
                jack.tumble();
                cout << jill.getLastRoll() << "\t\t" << jack.getPosition();
                cout << "\t\t" << jill.getPosition() << "\t\t" << "Tumble Jack!" << "\n";
            }
            
            //print for when tumble isn't necessary
            else {
                cout << jill.getLastRoll() << "\t\t\t\t" << jill.getPosition() << "\n";
            }
            
        } while (jack.getPosition() <= 20 && jill.getPosition() <= 20);
        
        gameCount+=1;
        
        //print which player won, and add the wins to their tally
        if (jill.getPosition() > jack.getPosition()) {
            cout << "JILL WINS GAME: " << gameCount << "\n\n";
            jill.win();
        }
        else {
            cout << "JACK WINS GAME: " << gameCount << "\n\n";
            jack.win();
        }
        
        //reset player positions
        jack.resetPosition();
        jill.resetPosition();
    }
    cout << "Jack won " << jack.getGamesWon() << " games ";
    cout << "and Jill won " << jill.getGamesWon() << " games!\n";
    
    if (jill.getGamesWon() > jack.getGamesWon()) {
        cout << "Jill is the Dice Race Grand Champion!";
    }
    else {
        cout << "Jack is the Dice Race Grand Champion!\n\n  ";
    }
}

int main() {
    
    //calls the method that prints the game
    printBoard();
    
    cin.ignore();  //keep output window open
    return EXIT_SUCCESS;  //return a value on exit
}
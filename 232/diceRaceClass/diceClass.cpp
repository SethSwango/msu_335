/*
Name: Joshua Yamdogo
Date: 2/19/2016
Assignment: ASN 3 - Dice Race Class
Platform/IDE: Windows 10, Visual Studio 2015
Description: The purpose of this assignment is to practice and develop skills related to classes, class methods, and their objects.
			 This will be accomplished by modifying Asn2 (Dice Race) to incorporate classes.
			 Operator overloading will also be used in this assignment.
*/

#include <iostream>
#include <string>
#include <ctime>

using namespace std;


// Class Player declaration.
class Player {
	public:
		Player();
		~Player();

		// Class methods.
		void printHeader(); //method to print the game board header
		int roll(); //method to roll the die
		void moveTo(int p);	//method to advance player position based on die roll
		void tumble(); //method to tumble back a player a random 1-3 squares
		void printBoard(int check, int roll, Player& jack, Player& jill); //method to print the entire game board
		void wins(int n); //method which totals the number of wins that each Player has
		int getWins(); //method to return the number of wins that each Player has
		bool gameWon(); //method to determine if the game has been won
		void clearBoard(); //method which clears the player position
		static void overallWinner(Player& p1, Player& p2); //method to print overall game winner

		// Override the boolean comparison function
		friend bool operator ==(Player& lhs, Player& rhs);

	private:
		int position;
		int totalWins;
		int const HILLTOP = 20; 

};

//Default constructor
Player::Player() {
	position = 0; //initial position of 0
	totalWins = 0;  //initial wins of 0
}

//Default destructor
Player::~Player() {}

//roll() simulates a dice with numbers 1-6.
int Player::roll() {
	int r = rand() % 6 + 1;
	return r;
}

//moveTo() allows the player to nagivate the board.
void Player::moveTo(int p) {
	position += p;
}

//tumble() moves the player back a random 1-3 places by passing a negative value to moveTo().
void Player::tumble() {
	int tumble = rand() % 3 + 1;
	moveTo(-tumble);
}

//clearBoard() resets the board for new games
void Player::clearBoard() {
	position = 0;
}

//Boolean definition to determine if a player has reached the end of the game
bool Player::gameWon() {
	return position >= HILLTOP;
}
//Define function that gets the wins for the player
int Player::getWins() {
	return totalWins;
}

//wins() adds total wins to the player score
void Player::wins(int n) {
	totalWins += n;
}

//printHeader() prints the header for the game board
void Player::printHeader() {
	cout << "Roll" << "\t" << "Jack" << "\t" << "Jill" << endl;
	cout << "------------------------------------" << endl;
}

//printBoard() allows the player to nagivate the board.
void Player::printBoard(int boardStatement, int roll, Player& jack, Player& jill) {
	switch (boardStatement) {
	case 1: 
		cout << roll << "\t" << jack.position << "\t" << jill.position << "\t" << "Tumble Jill!" << "\n";
		break;
	case 2:
		cout << roll << "\t" << jack.position << "\n";
		break;
	case 3:
		cout << roll << "\t" << jack.position << "\t" << jill.position << "\t" << "Tumble Jack!" << "\n";
		break;
	case 4:
		cout << roll << "\t" "\t" << jill.position << "\n";
		break;
	}
}

//overallWinner() prints the player with the most wins to name them the champion.
void Player::overallWinner(Player& p1, Player& p2) {
	cout << "Jack won " << p1.getWins() << " games and Jill won " << p2.getWins() << " games \n";
	if (p1.getWins() > p2.getWins()) {
		cout << "Jack is the Dice Race Grand Champion!";
	}
	else {
		cout << "Jill is the Dice Race Grand Champion!";
	}
}

//Override == operator to compare player positions
bool operator == (Player& lhs, Player& rhs) {
	return lhs.position == rhs.position;
}

/***	MAIN	***/
int main() {
	// Declare Player objects
	Player jack; 
	Player jill;

	int nGames = 2;
	// Get input for how many times the game should be played from the user. Only break out of the loop if they enter an odd number. 
	while (nGames % 2 == 0) {
		cout << "Please enter an odd amount of times to run the game: ";
		cin >> nGames;
	}

	// Variable to keep track of the winner when the for loop is done executing.
	string winner;

	// Seed random number generator & declare roll variable.
	srand(time(NULL));
	int roll;

	// Runs the for loop the exact number of times the user enters. Start at 1 to print "game 1" instead of "game 0".
	for (int i = 1; i < nGames+1; ++i) {
		bool finished = false;
		jack.printHeader();
		do {
			// Jack's turn.

				// Roll dice for Jack and add to his total.
				roll = jack.roll();
				jack.moveTo(roll);

				// If Jack rolls the same as Jill, move Jill back a random 1-3 squares and print the result. Else, print Jack's position. 
				if (jack == jill) {
					jill.tumble();
					jack.printBoard(1, roll, jack, jill);
				}
				else {
					jack.printBoard(2 ,roll, jack, jill);
				}

				// If Jack's score is greater than or equal to 20, declare Jack the winner and end the program.
				if (jack.gameWon()) {
					finished = true;
					jack.wins(1);
					winner = "Jack";
				}

			// Jill's turn.
				if (!jack.gameWon()) {

					// Roll dice for jill and add to her total.
					roll = jill.roll();
					jill.moveTo(roll);

					// If Jill rolls the same as Jack, move Jack back a random 1-3 squares and print the result. Else, print Jill's position. 
					if (jill == jack) {
						jack.tumble();
						jill.printBoard(3, roll, jack, jill);
					}
					else {
						jill.printBoard(4, roll, jack, jill);
					}

					// If Jill's score is greater than or equal to 20, declare Jill the winner to end the loop.
					if (jill.gameWon()) {
						finished = true;
						jill.wins(1);
						winner = "Jill";
					}
				}

		} while (!finished);

		// Print the game winner & clear the board
		jack.clearBoard();
		jill.clearBoard();
		cout << winner << " wins game " << i << "!\n" "\n";
	}
	
	// Print the overall winner between the games played.
	Player::overallWinner(jack, jill);

	cin.ignore(); //keep output window open return
	cin.ignore();
	return 0;   //return success
}
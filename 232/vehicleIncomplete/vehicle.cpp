/*
Name: Joshua Yamdogo
Date: 2/11/2016
Assignment: Lab 3 - Vehicle Class
Platform/IDE: Windows 10, Visual Studio 2015
Description: The purpose of this lab is to learn correct placement of components (i.e. declarations) within the program structure.
			 This objective will be accomplished by implementing a class named Vehicle using the point class as an example.
*/

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::ostream;
using std::string;

// Class Vehicle declaration
class Vehicle {
	public:
	Vehicle();  //Default constructor
	Vehicle(string make, string  model, int year, bool manual);  //vehicle info constructor
	~Vehicle();  //destructor
	
	// Setters.
		void setMake(string newMake);
		void setModel(string newModel);
		void setYear(int newYear);
		void setManual(bool newManual);

		// For << overload; both must be friends for access to private class fields.
		friend ostream& operator <<(ostream& out, Vehicle& v);

		// For == overload
		bool operator== (const Vehicle& rhs) const; 

	// Class fields
		private:
			string make;
			string model;
			int year;
			bool manual;
};

/* Class Vehicle - Definitions (methods) */

// Default Constructor; fields must *all* be initialized.
Vehicle::Vehicle() {
	make = " ";
	model = " ";
	year = 0;
	manual = false;
}

// 'Vehicle' constructor; fields must *all* be initialized.
Vehicle::Vehicle(string makeX, string modelX, int yearX, bool manualX) {
	make = makeX;
	model  = modelX;
	year = yearX;
	manual = manualX;
}

// Destructor
Vehicle::~Vehicle() {  }

/*** CLASS MEMBER METHODS ***/

// Setters for each value
	// Set make's field value.
	void Vehicle::setMake(string newMake) {
		make = newMake;
	}

	// Set model's field value.
	void Vehicle::setModel(string newModel) {
		model = newModel;
	}

	// Set year's field value.
	void Vehicle::setYear(int newYear) {
		year = newYear;
	}

	// Set manual field value.
	void Vehicle::setManual(bool newManual) {
		manual = newManual;
	}

// Overload for "is equal to" (==).
bool Vehicle::operator== (const Vehicle& rhs) const {
	if ((model == rhs.model) && (make == rhs.make) && (year == rhs.year) && (manual == rhs.manual))
		return true;
	else
		return false;
}

// Overloead for << to print out fields.
ostream& operator<<(ostream& ost, Vehicle& v) {
	cout << v.make << ", " << v.model << ", " << v.year << ", " << std::boolalpha << v.manual;
	return ost;
}

/***	MAIN	***/
int main() {
	Vehicle v;  //calls default constructor
	Vehicle v1("Ford", "Focus", 2005, true);  //calls Vehicle constructor
	Vehicle v2("Ford", "Focus", 2005, true);  //for testing ==
	Vehicle v3("Dodge", "Dart", 2013, false);  //for testing == & set methods

	// Using set methods to change v3.
	v3.setMake("Ford");
	v3.setModel("Focus");
	v3.setYear(2005);
	v3.setManual(true);

	// Test == overload.
	if (v == v1)
		cout << "Vehicle v equals Vehicle v2\n";  //will not cout as they are not equal
	if (v1 == v2)
		cout << "Vehicle v1 equals Vehicle v2\n";  //should return true
	if (v2 == v3)
		cout << "Vehicle v2 equals Vehicle v3\n";  //should return true since the values of v3 were changed

	// Test << overload.
	cout << "The make, model, year, and manual status is: " << v1 << "\n";

	getchar();  //pause output window
	return 0;   //return success
}
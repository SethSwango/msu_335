/*
Name: Joshua Yamdogo
Date: 2/23/2016
Assignment: Lab 4 - Vehicle Class
Platform/IDE: Windows 10, Visual Studio 2015
Description: This lab will implement a copy assignment and copy intructor methods to 'complete' the Vehicle class. 
			 This lab will also utilize the destructor for resource management, implement a child class, and test workinngs of inheritance. 
*/

#include <iostream>
#include <string>

using std::cout;
using std::ostream;
using std::string;

using namespace std;


// Class Vehicle declaration
class Vehicle {
	public:
	Vehicle();  //Default constructor
	Vehicle(string make, string  model, int year, bool manual);  //vehicle info constructor
	Vehicle(const Vehicle &v);  // copy assignment constructor declaration
	Vehicle& operator= (const Vehicle& rhs); //= assignment overload
	~Vehicle();  //destructor
	
	// Setters.
		virtual void setMake(); //virtual method accessed by child class
		void setModel(string newModel);
		void setYear(int newYear);
		void setManual(bool newManual);

		// For << overload; both must be friends for access to private class fields.
		friend ostream& operator <<(ostream& out, Vehicle& v);

		// For == overload
		bool operator== (const Vehicle& rhs) const; 

	//For destructor
		void deleteObject();
	// Class fields: private
		private:
			int year;
			bool manual;
	// Class fields: protected
	protected:
			string make;
			string model;
};

// Child class declaration
class Suv : public Vehicle {
	public:
		void setMake(); //method that parent class shares - redefined below

		// Attempt to access a private field from Vehicle - it is NOT allowed
		//int returnYear() { return year; }

};

/* Class Vehicle - Definitions (methods) */

// Default Constructor; fields must *all* be initialized.
Vehicle::Vehicle() {
	cout << "Default constructor now called \n";
	make = "None";
	model = "None";
	year = 0;
	manual = false;
}

// 'Vehicle' constructor; fields must *all* be initialized.
Vehicle::Vehicle(string makeX, string modelX, int yearX, bool manualX) {
	cout << "Vehicle constructor now called \n";
	make = makeX;
	model  = modelX;
	year = yearX;
	manual = manualX;
}

// Copy constructor
Vehicle::Vehicle(const Vehicle &v) :
	make(v.make), model(v.model), year(v.year), manual(v.manual) {
	cout << "Copy constructor now called \n";
}

// = assignment overload
Vehicle& Vehicle::operator= (const Vehicle& rhs) {
	cout << "Assignment = overload now called \n";
	if (this != &rhs) {
		make = rhs.make;
		model = rhs.model;
		year = rhs.year;
		manual = rhs.manual;
	}
	return *this;

}

// Destructor
Vehicle::~Vehicle() { 
	cout << "Destructor has been called \n";
}

/*** VEHICLE CLASS MEMBER METHODS ***/

// Setters for each value
	// Set make's field value.
	void Vehicle::setMake() {
		cout << "Parent class setMake() virtual method has been called \n";
		make = "CAR";
	}

	// Set model's field value.
	void Vehicle::setModel(string newModel) {
		model = newModel;
	}

	// Set year's field value.
	void Vehicle::setYear(int newYear) {
		year = newYear;
	}

	// Set manual field value.
	void Vehicle::setManual(bool newManual) {
		manual = newManual;
	}

// Overload for "is equal to" (==).
bool Vehicle::operator== (const Vehicle& rhs) const {
	if ((model == rhs.model) && (make == rhs.make) && (year == rhs.year) && (manual == rhs.manual))
		return true;
	else
		return false;
}

// Overload for << to print out fields.
ostream& operator<<(ostream& ost, Vehicle& v) {
	cout << v.make << ", " << v.model << ", " << v.year << ", " << std::boolalpha << v.manual;
	return ost;
}

/// Calls destructor - unesscessary?
///	void Vehicle::deleteObject() {
///		delete this;
///	}

/*** MyNewVehicle CLASS MEMBER METHODS ***/

// Redefine the setMake method from parent class virtual method
void Suv::setMake() {
	cout << "Child class setMake() method been called \n";
	make = "SUV";
}

// Function to take pointer to access the virtual method
void func(Vehicle *car) {
	car->setMake();
}


/***	MAIN	***/
int main() {
	cout << "Step one: The default constructor will be called for Vehicle v \n";
	Vehicle v;  //calls default constructor

	cout << "\nStep two: The constructor will be called for Vehicle v1, v2, v3 \n";
	Vehicle v1("Ford", "Focus", 2005, true);  //calls Vehicle constructor
	Vehicle v2("Ford", "Focus", 2005, true);  //for testing ==
	Vehicle v3("Dodge", "Dart", 2013, false);  //for testing == & set methods

	cout << "\nStep three: v4, created using the copy constructor, will be printed. \n";
	Vehicle v4 = v3; //this calls the copy instructor, not the assignment overload. v4 is not initialized. 
	cout << "Printing v4: " << v4 << "\n";

	cout << "\nStep four: v5, created using assignment overload, will be printed.\n";
	Vehicle v5; //must initialize object in order to use assigment overload
	v5 = v1; //this calls assignment overload
	cout << "Printing v5: " << v5 << "\n";

	cout << "\nStep five: Create child class object and parent class object: mySuv and myCar.\n";
	Suv *mySuv = new Suv; //create child class object
	Vehicle *myCar = new Vehicle; //create parent class object

	cout << "\nStep six(A): Call virtual method on Suv object mySuv\n";
	func(mySuv); //this will call the setMake() method in the child class which has been redefined
	cout << "Printing mySuv: " << *mySuv << "\n";

	cout << "\nStep six(B): Call virtual method on Vehicle object myCar\n";
	func(myCar); //this will call the virtual setMake() method in the parent class 
	cout << "Printing myCar: " << *myCar << "\n";

	cout << "\nStep seven: Attempt to access a parent private field from a child method.\n";
	//mySuv->returnYear();
	cout << "You CANNOT access a parent PRIVATE field from a child method. Not allowed.";

	/// Resutls in heap error?
	///v5.deleteObject();

	/// Reduce output clutter. The following code is not really needed for Lab 4.
		/*
		cout << "\nSet methods are used to alter the value of v3\n";
		// Using set methods to change v3.
		v3.setMake("Ford");
		v3.setModel("Focus");
		v3.setYear(2005);
		v3.setManual(true); 
		
		cout << "\nTest the == overload by comparing v to v2, v1 to v2, and v2 to v3\n";
		// Test == overload.
		if (v == v1)
			cout << "Vehicle v equals Vehicle v2\n";  //will not cout as they are not equal
		if (v1 == v2)
			cout << "Vehicle v1 equals Vehicle v2\n";  //should return true
		if (v2 == v3)
			cout << "Vehicle v2 equals Vehicle v3\n";  //should return true since the values of v3 were changed

		cout << "\nTest the << overload by printing v1\n";
		// Test << overload.
		cout << "The make, model, year, and manual status is: " << v1 << "\n"; 
		*/

	getchar();  //pause output window
	return 0;   //return success
}

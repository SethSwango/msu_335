/*
 Name: Seth Swango
 
 Date: 2016/02/24
 Assignment: Complete Vehicle Class
 Platform/IDE: OSX/Xcode7
 
 Description: This lab explores the basic principles of creating object classes in c++. In this program a class object that stores the attributes of a real world automible is defined, as well as the functions necessary to modify these attributes. It expands on the initial vehicle class assignment by completing the class by defining a copy constructor, a copy assignment, cout's to all constructors and destructors and a child class.
 */

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::ostream;
using std::string;

// vehicle class
class Vehicle {
public:
    
    Vehicle();  //default constructor
    Vehicle(string make, string  model, string year, bool manual);  //constructor
    ~Vehicle();  //destructor
    
    // setters
    virtual void setYear(string newYear);
    void setMake(string newMake);
    void setModel(string newModel);
    void setManual(bool newManual);
    
    // << overload
    friend ostream& operator <<(ostream& out, Vehicle& v);
    
    // == overload
    bool operator== (const Vehicle& rhs) const;
    
    // Copy Constructor
    Vehicle(const Vehicle& v);
    
    // Assignment Overload
    Vehicle& operator= (const Vehicle& rhs);
    
    // class fields
private:
    string make;
    string model;
    
protected:
    string year;
    bool manual;
};

// child class
class Coupe : public Vehicle {
public:
    void setYear(string newYear); //shared method
    
    //child getter function
    //string getModel() { return make; }
    //doesn't work because child classes can't access parent's private fields
    
};

//child class redifinition of virtual method setYear()
void Coupe::setYear(string newYear) {
    cout << "child setYear method called\n";
    year = newYear;
}

// Function to take pointer to access the virtual method
void virtualSetYear(Vehicle *v) {
    string newYear= "1324";
    v->setYear(newYear);
}

// Destructor
Vehicle::~Vehicle() {
    cout << "destructor has been called\n";
}

// default constructor
Vehicle::Vehicle() {
    cout << "default constructor has been called\n";
    make = "None";
    model = "None";
    year = "0";
    manual = false;
}

// constructor
Vehicle::Vehicle(string aMake, string aModel, string aYear, bool aManual) {
    cout << "initializer constructor has been called\n";
    make = aMake;
    model  = aModel;
    year = aYear;
    manual = aManual;
}

// setters
void Vehicle::setMake(string aMake) {
    make = aMake;
}

void Vehicle::setModel(string aModel) {
    model = aModel;
}

void Vehicle::setYear(string newYear) {
    cout << "virtual method setYear() has been called\n";
    year = newYear;
}

void Vehicle::setManual(bool aManual) {
    manual = aManual;
}

// overload for "==" operator
bool Vehicle::operator== (const Vehicle& rhs) const {
    return ((model == rhs.model) && (make == rhs.make) && (year == rhs.year) && (manual == rhs.manual));
}

// overload for "cout <<" operator
ostream& operator<<(ostream& ost, Vehicle& v) {
    cout << v.make << ", " << v.model << ", " << v.year << ", " << std::boolalpha << v.manual;
    return ost;
}

// copy constructor
Vehicle::Vehicle(const Vehicle& v)
    : make(v.make), model(v.model), year(v.year), manual(v.manual) {
    cout << "copy constructor has been called\n";
}

// overload for assignment operator
Vehicle& Vehicle::operator= (const Vehicle& rhs) {
    cout << "assignment overload has been called\n";
    make = rhs.make;
    model = rhs.model;
    year = rhs.year;
    manual = rhs.manual;
    return *this;
}

// copy

int main() {
    cout << "====== prints for the original vehicle class lab ======\n\n";
    
    Vehicle v_instance0;
    Vehicle v_instance1("Alfa Romeo", "Juilet", "1984", true);
    Vehicle v_instance2("Alfa Romeo", "Juilet", "1984", true);
    
    // exposition of the "==" operator overload method
    if (v_instance0 == v_instance1)
        cout << "v_instance0 == v_instance1\n"; //prints nothing because the vehicle objects are different
    if (v_instance1 == v_instance2)
        cout << "v_instance1 == v_instance2\n";  //if statement is satisfied, because vehicle objects are equal
    
    // set attributes for vehcile 2
    v_instance2.setYear("1980");
    v_instance2.setManual(true);
    v_instance2.setMake("Ford");
    v_instance2.setModel("Pinto");
    
    // test the "==" operator overload again, now that the vehcile objects are no longer equal
    if (v_instance1 == v_instance2)
        cout << "v_instance1 == v_instance2 \n";  //if statement is not satisfied
    
    // "<<" operator exposition
    cout << "Info for v_instance2 after using setters to change attributes: " << v_instance2 << "\n\n";
    
    cout << "====== prints for the comlete vehicle class lab ======\n\n";
    
    // copy constructor exposition
    Vehicle v_copy = v_instance2;
    cout << "Attributes of copied vehicle object: " << v_copy << "\n\n";
    
    // assignment overload exposition
    Vehicle v_assigned;
    v_assigned = v_instance2;
    cout << "Attributes of the vehicle object after assignment are: " << v_assigned << "\n\n";
    
    // create child and parent vehicle objects
    Coupe *aCoupe = new Coupe; //create child
    Vehicle *aVehicle = new Vehicle; // create parent
    
    // call virtual method setYear() on child object
    virtualSetYear(aCoupe);
    cout << "child object attributes after virtualSetYear() has been called on it: " << *aCoupe << "\n\n";
    
    // call virutal method setYear on parent class object
    virtualSetYear(aVehicle);
    cout << "parent object attributes after virtualSetYear() has been called on it: " << *aCoupe << "\n\n";
    
    // attempt to access parent's private fields
    cout << "you can't access a parent's private fields from a child method";
    
    
    getchar();  //pause output window
    return 0;   //return success
}
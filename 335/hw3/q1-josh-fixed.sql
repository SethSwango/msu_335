/*
Name: Joshua Yamdogo
Date: 2/16/2016
Assignment: Homework 3
Folder: //trace/CSC-335/001/Yamdogo123/HW3/q1.sql
*/

/* 1. Exercise 3.20 page 110. Include foreign key constraints where needed. 
Notice that the relation manages is recursive between employee and employee (similar to the relations prereq and course in the university database.) 
Also notice that both employee_name and manager_name are foreign keys in the relation manages referencing the relation employee. Be careful about the order you create the tables. */

create table employee
	(employee_name varchar2(20),
	 street varchar2(20), 
	 city varchar2(20), /*using varchar because "Washington D.C." is in the data*/
	 primary key (employee_name),
	);
 
create table company
	(company_name varchar2(25),
	 city varchar2(20)
	 primary key (company_name),
	);

create table works
	(employee_name varchar2(20),
	company_name varchar2(25),
	salary numeric(10,2), /*using 10,2 because of large numerical values in the data*/
	primary key (employee_name),
	FOREIGN KEY company_name REFERENCES company(company_name)
	);

create table manages
	(employee_name varchar2(20),
	 manager_name varchar2(20),
	 FOREIGN KEY employee_name REFERENCES employee(employee_name),
	 FOREIGN KEY manager_name REFERENCES employee(employee_name),
	);



/*
Name: Joshua Yamdogo
Date: 2/16/2016
Assignment: Homework 3
Folder: //trace/CSC-335/001/Yamdogo123/HW3/q1.sql
*/

/* 1. Exercise 3.20 page 110. Include foreign key constraints where needed. 
Notice that the relation manages is recursive between employee and employee (similar to the relations prereq and course in the university database.) 
Also notice that both employee_name and manager_name are foreign keys in the relation manages referencing the relation employee. Be careful about the order you create the tables. */

create table employee
	(employee_name varchar(20) PRIMARY KEY,
	 street varchar(20), 
	 city varchar(20) /*using varchar because "Washington D.C." is in the data*/
	);
 
create table company
	(company_name varchar(25) PRIMARY KEY,
	 city varchar(20)
	);

create table works
	(employee_name varchar(20) PRIMARY KEY,
	company_name varchar(25) FOREIGN KEY REFERENCES company(company_name),
	salary numeric(10,2), /*using 10,2 because of large numerical values in the data*/
	);

create table manages
	(employee_name varchar(20) FOREIGN KEY REFERENCES employee(employee_name),
	 manager_name varchar(20) FOREIGN KEY REFERENCES employee(employee_name),
	);



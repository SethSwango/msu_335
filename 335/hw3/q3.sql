/*
Name: Joshua Yamdogo
Date: 2/16/2016
Assignment: Homework 3
Folder: //trace/CSC-335/001/Yamdogo123/HW3/q3.sql
*/

/* 3. Exercise 3.16 page 109. */

/* a. Find the names of all employees who work for "First Bank Corporation." */
select employee_name
from works
where company_name = 'First Bank Corporation';

/* b. Find all employees in the database who live in the same cities as the companies for which they work. */
select employee.employee_name
from employee, works, company 
where employee.employee_name = works.employee_name and 
      employee.city = company.city and 
      works.company_name = company.company_name;

/* c. Find all employees in the database who live in the same cities and steets as do their managers. */
select E.employee_name
from employee E, employee X, manages M
where E.employee_name = M.employee_name and
      M.manager_name = X.employee_name and
      E.street = X.street and
      E.city = X.city;


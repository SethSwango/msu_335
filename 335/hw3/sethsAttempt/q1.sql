/* Seth Swango -- HW 1 -- Q1*/

create table employee(
  employee_name varchar2(25),
  street varchar2(25),
  city varchar2(25),
  primary key (employee_name)
  );

create table company(
  company_name varchar2(25),
  city varchar2(25),
  primary key (company_name)
  );

create table works(
  employee_name varchar2(25),
  company_name varchar2(25),
  salary decimal(9,2),
  primary key (employee_name),
  foreign key (employee_name) references employee,
  foreign key (company_name) references company);

create table manages(
  employee_name varchar2(25),
  manager_name varchar2(25),
  primary key (employee_name),
  foreign key (employee_name) references employee,
  foreign key (manager_name) references employee);

/* Seth Swango -- HW 1 -- Q3*/

select employee_name 
 from works 
 where company_name= 'First Bank Corporation';

select works.employee_name 
 from employee 
 join works on employee.employee_name = works.employee_name 
 join company on company.company_name = works.company_name 
 where company.city= employee.city;

select employee_name 
 from manages 
 natural join employee 
 where employee.city = (select city from employee where employee_name = manager_name) and 
 employee.street = (select street from employee where employee_name = manager_name);

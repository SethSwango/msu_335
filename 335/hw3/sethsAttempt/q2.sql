/* Seth Swango -- HW 1 -- Q2*/

insert into employee values ('Seth','Jefferson','Springfield');

insert into employee values ('Jay','National','Springfield');

insert into employee values ('Jack','National','Springfield');

insert into employee values ('John','Grand','Springfield');

insert into company values ('First Bank Corporation','Springfield');

insert into company values ('Bass Pro Shops','St. Louis');

insert into works values ('Seth','Bass Pro Shops','9000');

insert into works values ('Jay','First Bank Corporation','9500');

insert into works values ('Jack','First Bank Corporation', '10342');

insert into works values ('John','Bass Pro Shops','5233');

insert into manages values ('John','Seth');

insert into manages values ('Jay','Jack');
